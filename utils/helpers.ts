export function objToBase64(obj: any) {
	return strToBase64(JSON.stringify(obj));
}

export function base64ToObj(base64: string) {
	return JSON.parse(base64ToStr(base64));
}

export function strToBase64(str: string) {
	return Buffer.from(str).toString("base64");
}

export function base64ToStr(base64: string) {
	return Buffer.from(base64, "base64").toString();
}

export function debounce(fn: Function, ms = 300) {
	let timeoutId: ReturnType<typeof setTimeout>;
	return function (this: any, ...args: any[]) {
		clearTimeout(timeoutId);
		timeoutId = setTimeout(() => fn.apply(this, args), ms);
	};
}

export function uuidv4() {
	// @ts-ignore
	return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
		(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
	);
}
