import {base64ToStr} from '~/utils/helpers';

const forge = require("node-forge");

export interface RSAKey {
	privateKey: string;
	publicKey: string;
}

export class RSA {
	generateKeys(): Promise<RSAKey> {
		return new Promise((resolve, reject) => {
			forge.pki.rsa.generateKeyPair({bits: 4096, workers: -1}, function(err: any, keypair: any) {
				resolve({
					publicKey: forge.pki.publicKeyToPem(keypair.publicKey),
					privateKey: forge.pki.privateKeyToPem(keypair.privateKey)
				});
			});
		});
	}
	decrypt(key: string, message: string): Promise<string> {
		return new Promise((resolve, reject) => {
			const privateKey = forge.pki.privateKeyFromPem(key);
			resolve(privateKey.decrypt(base64ToStr(message)));
		});
	}
	encrypt(key: string, message: string): Promise<string> {
		return new Promise((resolve, reject) => {
			const publicKey = forge.pki.publicKeyFromPem(key);
			resolve(publicKey.encrypt(base64ToStr(message)));
		});
	}
}
