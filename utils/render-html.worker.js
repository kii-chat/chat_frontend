import forge from "node-forge";
import {base64ToStr} from "~/utils/helpers";

onmessage = ({ data: { key, message } }) => {
	const privateKey = forge.pki.privateKeyFromPem(key);
	self.postMessage(privateKey.decrypt(base64ToStr(message)));
};
