import {base64ToStr, strToBase64} from '~/utils/helpers';

const forge = require("node-forge");

export interface Hashes {
	salt: string;
	key: string;
}

export function generateLoginPassword(password: string): string {
	const md = forge.md.sha512.create();
	md.update(password);
	return md.digest().toHex();
}

export function generateVaultKeyAndSalt(password: string): Promise<Hashes> {
	return new Promise((resolve, reject) => {
		const salt = forge.random.getBytesSync(128);
		const key = forge.pkcs5.pbkdf2(password, salt, 10_000, 32);
		resolve({
			salt: strToBase64(salt),
			key: key
		});
	})

}

export function generateVaultKeyWithSalt(salt: string, password: string): Promise<string> {
	return new Promise((resolve, reject) => {
		resolve(forge.pkcs5.pbkdf2(password, base64ToStr(salt), 10_000, 32));
	});
}
