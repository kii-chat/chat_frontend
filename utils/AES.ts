import {base64ToObj, base64ToStr, objToBase64, strToBase64} from "~/utils/helpers";

const forge = require("node-forge");

export interface AESContent {
	iv: string;
	content: string;
}

export class AES {
	encrypt(key: string, content: string): Promise<string> {
		return new Promise((resolve, reject) => {
			const cipher = forge.cipher.createCipher("AES-CBC", key);
			const iv = forge.random.getBytesSync(128);

			cipher.start({ iv });
			cipher.update(forge.util.createBuffer(content));
			cipher.finish();

			const data: AESContent = {
				iv: strToBase64(iv),
				content: strToBase64(cipher.output.getBytes())
			};

			resolve(objToBase64(data));
		});
	}

	decrypt(key: string, encrypted: string): Promise<string> {
		return new Promise((resolve, reject) => {
			const data: AESContent = base64ToObj(encrypted);

			const iv = base64ToStr(data.iv);
			const content = base64ToStr(data.content);

			const decipher = forge.cipher.createDecipher("AES-CBC", key);
			decipher.start({ iv });
			decipher.update(forge.util.createBuffer(content));
			decipher.finish();

			resolve(decipher.output.toString());
		});
	}
}
