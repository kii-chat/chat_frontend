import {base64ToStr, uuidv4} from "@/utils/helpers";
import forge from "node-forge";

export default ({ store, env }, inject) => {
	inject('ws', {
		start() {
			const url = env.wsUrl;
			const chat = new WebSocket(url+'ws/chat/');
			const chatlist = new WebSocket(url+'ws/chatlist/');
			const pki = forge.pki;
			chat.onopen = function () {
				const token = sessionStorage.getItem("token");
				chat.send(JSON.stringify({
					token: token,
					action: "subscribe_to_message_user_from_user",
					request_id: uuidv4()
				}));
			}
			chatlist.onopen = function () {
				const token = sessionStorage.getItem("token");
				chatlist.send(JSON.stringify({
					token: token,
					action: "subscribe_to_chat_user_from_user",
					request_id: uuidv4()
				}));
			}
			chat.onmessage = function (e) {
				const message = JSON.parse(e.data);
				if(message.action === "create") {
					const body = message.body;

					const key = sessionStorage.getItem("privateKey");
					const privateKey = pki.privateKeyFromPem(key);

					const decrypt = privateKey.decrypt(base64ToStr(body.encrypted_message));

					if(body.sender !== store.state.general.username) {
						$nuxt.$emit('chat-list');
						$nuxt.$emit('chat-update.'+body.chat, {
							message: decrypt
						});
						var notification = new Notification(body.sender, { body: decrypt });
						notification.onclick = (event) => {
							event.preventDefault();
							window.open('/chat/'+body.chat, '_blank');
						}
					}
				}
			};
			chatlist.onmessage = function (e) {
				$nuxt.$emit('chat-list-update');
			};
			chat.onclose = function (e) {
			};
			chatlist.onclose = function (e) {
			};
		}
	})
}
