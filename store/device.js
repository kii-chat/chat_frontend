export const state = () => ({
	deviceId: ''
})

export const mutations = {
	login(state, deviceId) {
		state.deviceId = deviceId;
	}
}
