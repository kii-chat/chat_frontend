export const state = () => ({
	token: null,
	privateKey: null
})

export const mutations = {
	login(state, payload) {
		state.token = payload.token;
		state.privateKey = payload.privateKey;
	},
	logout(state) {
		state.token = null;
		state.privateKey = null;
	}
}
