import forge from "node-forge";

onmessage = (event) => {
	console.log(event);
	const { privateKey, message } = event;
	postMessage(privateKey.decrypt(Buffer.from(message, "base64").toString()));
};
