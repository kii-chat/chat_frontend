export default {
	ssr: false,
	target: "static",
	head: {
		title: "chat",
		meta: [
			{ charset: "utf-8" },
			{ name: "viewport", content: "width=device-width, initial-scale=1" },
			{ hid: "description", name: "description", content: "" },
			{ name: "format-detection", content: "telephone=no" }
		],
		link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
	},
	css: [
		"~/assets/style.css"
	],
	plugins: [
		"~/plugins/ws"
	],
	components: true,
	buildModules: [
		"@nuxt/typescript-build",
		"@nuxtjs/style-resources",
		[
			'@nuxtjs/vuetify',
			{
				theme: {
					themes: {
						light: {
							primary: '#FF1616',
							secondary: '#b0bec5',
							anchor: '#8c9eff',
							success: '#FF1616'
						},
					},
				},
			}
		]
	],
	loading: {
		color: '#FF1616',
		height: '2px'
	},
	modules: [
		"@nuxtjs/axios",
		"@nuxtjs/pwa",
		"~/modules/worker.js"
	],
	axios: {
		baseURL: 'https://testapi.kii-chat.de/'
	},
	pwa: {
		manifest: {
			lang: "en"
		}
	},
	build: {},
	vuetify: {
		customVariables: ['~/assets/variables.scss'],
		treeShake: true
	},
	env: {
		apiUrl: 'https://testapi.kii-chat.de/',
		wsUrl: 'wss://testapi.kii-chat.de/'
	}
};
