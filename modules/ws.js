const WebSocket = require('ws')
const ws = new WebSocket('ws://localhost:8000/ws/call/room/');

ws.onopen = function () {
	this.send(JSON.stringify({
		pk: room_pk,
		action: "join_room",
		request_id: request_id
	}));
	this.send(JSON.stringify({
		pk: room_pk,
		action: "retrieve",
		request_id: request_id
	}));
	this.send(JSON.stringify({
		pk: room_pk,
		action: "subscribe_to_calls_in_calls_room",
		request_id: request_id
	}));
	this.send(JSON.stringify({
		pk: room_pk,
		action: "subscribe_instance",
		request_id: request_id
	}));
};

function setMessages(item) {
	if (item) {

	}
}

ws.onmessage = function (e) {
	const message = JSON.parse(e.data);
	switch (message.action) {
		case "retrieve":
			setMessages(message.data.messages);
			break;
		case "create":
			setMessages(message.data)
			break;
		default:
			break;
	}
};

ws.onclose = function (e) {
};

// wss.on('connection', ws => {
// 	ws.on('message', message => {
// 		console.log('received: %s', message);
// 	})
// 	ws.send('Hello')
// })
//
// export default function () {
// 	this.nuxt.hook('listen', server => {
// 		server.on('upgrade', (request, socket, head) => {
// 			wss.handleUpgrade(request, socket, head, ws => {
// 				wss.emit('connection', ws);
// 			})
// 		})
// 	})
// }
