export default function({ $axios, redirect }) {
	if(sessionStorage.getItem("token") == null) {
		redirect("/start");
	}
	$axios.onError(error => {
		if(error.response.status === 401) {
			sessionStorage.clear();
			redirect("/start");
		}
	})
}
