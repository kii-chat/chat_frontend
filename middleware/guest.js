export default function({ redirect }) {
	if(sessionStorage.getItem("token") != null) {
		redirect("/");
	}
}
